import { Component, OnInit } from "@angular/core";
import { CommonModule } from "@angular/common";
import {
    ColorPickerModule,
    Color,
    ColorPickerControl,
} from "@iplab/ngx-color-picker";

@Component({
    selector: "app-root",
    standalone: true,
    imports: [CommonModule, ColorPickerModule],
    template: `
        <h1 class="text-xl font-bold mb-3">Start Farbe</h1>
        <chrome-picker [control]="baseColorControl"></chrome-picker>
        <h1 class="mt-6 text-xl font-bold my-3">Erzeugte Farben</h1>
        <button
            class="border-slate-400 border px-2 py-1 bg-slate-300 rounded-xl m-1 hover:bg-slate-400"
            (click)="addColor()"
        >
            + Farbe hinzufügen
        </button>
        <button
            class="border-slate-400 border px-2 py-1 bg-slate-300 rounded-xl m-1 hover:bg-slate-400"
            (click)="removeColor()"
        >
            X Farbe löschen
        </button>
        <div class="md:flex">
            <div
                *ngFor="let colorGenerated of generatedColorControls"
                class="h-24 w-full md:h-96 md:mx-3 my-3 p-2 rounded-lg text-sm"
                [style.background-color]="colorGenerated.value.getRgba()"
            >
                <p>{{ colorGenerated.value.toRgbString() }}</p>
                <p>{{ colorGenerated.value.toCmykString() }}</p>
                <p>{{ colorGenerated.value.toHexString() }}</p>
            </div>
        </div>
    `,
    styles: [],
})
export class AppComponent implements OnInit {
    ngOnInit(): void {
        this.baseColorControl.valueChanges.subscribe(() => {
            this.generatePalette();
        });
    }
    title = "palette-generator";

    generatedColorControls: ColorPickerControl[] = [];
    baseColorControl = new ColorPickerControl()
        .setValueFrom("#f00")
        .hidePresets()
        .hideAlphaChannel();

    addColor() {
        this.generatedColorControls.push(
            new ColorPickerControl().setValueFrom(this.baseColorControl.value),
        );

        this.generatePalette();
    }

    removeColor() {
        this.generatedColorControls = this.generatedColorControls.slice(1);

        this.generatePalette();
    }

    generatePalette() {
        const hueDiff = 360.0 / this.generatedColorControls.length;

        for (
            let index = 0;
            index < this.generatedColorControls.length;
            index++
        ) {
            const generatedColorControl = this.generatedColorControls[index];

            const color = new Color("#000").setHsla(
                this.baseColorControl.value.getHsla().hue + index * hueDiff,
                this.baseColorControl.value.getHsla().saturation,
                this.baseColorControl.value.getHsla().lightness,
                1,
            );

            generatedColorControl.setValueFrom(color);
        }
    }
}
